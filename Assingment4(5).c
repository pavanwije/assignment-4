#include <stdio.h>
int main()
{
    int number,ans,x=1;
    printf("Enter Positive Integer = ");
    scanf("%d", &number);
    while (ans!= number*number)
    {

        printf("%d * %d = %d \n", number, x, number * x);
        ans=number*x;
        x=x+1;

    }

    return 0;
}

/*1 2 3 4 5
2 4 6 8 10
3 6 9 12 15
4
5*/
