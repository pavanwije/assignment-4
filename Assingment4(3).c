#include <stdio.h>
int main() {
    int number, a = 0, b;
    printf("Enter an integer: ");
    scanf("%d", &number);
    while (number != 0)
        {
        b = number % 10;
        a = a * 10 + b;
        number = number/10;
       }
    printf("Reversed number = %d", a);
    return 0;
}
